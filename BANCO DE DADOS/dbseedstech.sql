-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Dez-2017 às 00:20
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbseedstech`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` bigint(20) NOT NULL,
  `nome` varchar(70) NOT NULL,
  `usuario` varchar(80) NOT NULL,
  `senha` varchar(120) NOT NULL,
  `flAtivo` tinyint(1) NOT NULL DEFAULT '1',
  `flAdministrador` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `flCliente` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nome`, `usuario`, `senha`, `flAtivo`, `flAdministrador`, `email`, `telefone`, `flCliente`) VALUES
(12, 'Marcos Silva', 'administrador', '$2y$10$i2QwF8W4ev2kIRoHSS0MN.hOD9PwHQTOdU4aPu5.vaoHTz6nmQc66', 1, 1, 'teste@gmail.com', '(88) 9999-9999', 0),
(13, 'Manoel Silva', 'cliente', '$2y$10$9IS7/LmyTSkT6wLVo.UgPOxLgpIYQ.y.iB14MXzDSuSULLbDbRaVi', 1, 0, 'cliente@hotmail.com', '(88) 8888-8888', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
