-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Fev-2019 às 16:13
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbseedstech`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `idContato` bigint(20) NOT NULL,
  `nome` varchar(70) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `flRespondida` tinyint(1) NOT NULL DEFAULT '0',
  `flReceberEmail` tinyint(1) NOT NULL DEFAULT '1',
  `resposta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`idContato`, `nome`, `telefone`, `email`, `descricao`, `flRespondida`, `flReceberEmail`, `resposta`) VALUES
(3, 'Barreto', '88996972675', 'carlosbarreto1@gmail.com', 'Olá equipe seedstech, gostaria de uma proposta comercial detalhada para construção de um app mobile', 0, 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE `projeto` (
  `idProjeto` int(11) NOT NULL,
  `nome` varchar(25) NOT NULL,
  `dataInicio` date NOT NULL,
  `dataPrevistaFim` date NOT NULL,
  `idCliente` int(11) NOT NULL,
  `flFinalizado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`idProjeto`, `nome`, `dataInicio`, `dataPrevistaFim`, `idCliente`, `flFinalizado`) VALUES
(14, 'Projeto SeedsTech', '2017-12-03', '2017-12-31', 13, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefa`
--

CREATE TABLE `tarefa` (
  `idTarefa` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `flFinalizada` tinyint(1) NOT NULL,
  `dataTermino` datetime NOT NULL,
  `idProjeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tarefa`
--

INSERT INTO `tarefa` (`idTarefa`, `nome`, `flFinalizada`, `dataTermino`, `idProjeto`) VALUES
(16, 'Cadastro de cliente físico', 1, '2018-11-21 00:00:00', 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` bigint(20) NOT NULL,
  `nome` varchar(70) NOT NULL,
  `usuario` varchar(80) NOT NULL,
  `senha` varchar(120) NOT NULL,
  `flAtivo` tinyint(1) NOT NULL DEFAULT '1',
  `flAdministrador` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `flCliente` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nome`, `usuario`, `senha`, `flAtivo`, `flAdministrador`, `email`, `telefone`, `flCliente`) VALUES
(12, 'Marcos Silva', 'administrador', '$2y$10$i2QwF8W4ev2kIRoHSS0MN.hOD9PwHQTOdU4aPu5.vaoHTz6nmQc66', 1, 1, 'administrador@gmail.com', '(88) 9999-9999', 0),
(13, 'Manoel Silva', 'cliente', '$2y$10$9IS7/LmyTSkT6wLVo.UgPOxLgpIYQ.y.iB14MXzDSuSULLbDbRaVi', 1, 0, 'cliente@hotmail.com', '(88) 8888-8888', 1),
(14, 'bnmbnm', 'fgdfgdfg', '$2y$10$bKhEN9DqwlFq5T56y2H3guSlW.MV9m3S9NwaqoNnHS/4NeI/GeVt.', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(15, 'bnmbnm', 'fgdfgdfg', '$2y$10$YSzSCDSa3qWLrB9SiJVI9emfj..I4y9klMIDlW9AMP8/sb.vFqnE.', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(16, 'bnmbnm', 'fgdfgdfg', '$2y$10$3Y0tnK/zsEhI5pEdmiTzZOXCBUOHpBxdLrEn8i47J8q7ieNLztGe.', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(17, 'bnmbnm', 'fgdfgdfg', '$2y$10$ae6/j/kb3e9C/PVkB.pB6.MWs5fC7kDyvTfDn1xJbkCRiQX59A4z2', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(18, 'bnmbnm', 'fgdfgdfg', '$2y$10$8aDAjpG5OIay05qx.YOXUOeF9NO1RcSXdG8OITnIeIZy5WBCxFkia', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(19, 'bnmbnm', 'fgdfgdfg', '$2y$10$XNfW7Fm4BhqyKpVeAmIvbOFKhEzNce0mq4/e1Yr9qi8gwfEMz26jy', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(20, 'bnmbnm', 'fgdfgdfg', '$2y$10$GMgkfHNdYdlThE8Ycy5Imui8zf9UnfkANyztR7kjgMbrlPVe92xfy', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(21, 'bnmbnm', 'fgdfgdfg', '$2y$10$w.duackMN0jY5COnwXF4POU74dl5fZpjaqna/GhUU2hsI6vXheMOy', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(22, 'bnmbnm', 'fgdfgdfg', '$2y$10$q1RFD2H/t9FUiDWjVbeMeOLezGgytHoyREtZOtmLk67p4iVgg/w5S', 1, 0, 'tessstes@gmail.com', '(13) 2132-1321', 0),
(24, 'Maria da Silva Pereira', 'oipiop', '$2y$10$IYrVGdmf6knnF9uXxjaRFeuNNdXNS8g3vnHPUQFOZeyHbzJxyLTVK', 1, 0, '123123123@GMAIL.COM', '(13) 2132-1321', 0),
(25, 'Maria da Silva Pereira', 'oipiop', '$2y$10$FwrHqjA.6rJ9YwMwQnmmi.zNRCPfR8H9vBHYaYgGSVoWn9YQkJ3PW', 1, 0, '123123123@GMAIL.COM', '(13) 2132-1321', 0),
(26, 'Maria da Silva Pereira', 'oipiop', '$2y$10$IA2OyfepaRdsu.zxj.U5U.Uho/W.vdYdpUHbkYhG01DB4lPv8X2V2', 1, 0, '123123123@GMAIL.COM', '(13) 2132-1321', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`idContato`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`idProjeto`);

--
-- Indexes for table `tarefa`
--
ALTER TABLE `tarefa`
  ADD PRIMARY KEY (`idTarefa`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `idContato` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
  MODIFY `idProjeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tarefa`
--
ALTER TABLE `tarefa`
  MODIFY `idTarefa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
