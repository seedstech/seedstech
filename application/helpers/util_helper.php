<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Verifica se os dados da sessão é do tipo administrador
 * @param type $dados => Dados da sessão ($this->session->userdata()) 
 */
function verificarNivelAdm($dados) {

    if (!isset($dados['usuarioLogado'])) {
        redirect(base_url('login'));
    }

    if ($dados['flCliente']) {
        redirect(base_url('cliente'));
    }
}

/**
 * Verifica se os dados da sessão é do tipo cliente
 * @param type $dados => Dados da sessão ($this->session->userdata()) 
 */
function verificarNivelCliente($dados) {

    if (!isset($dados['usuarioLogado'])) {
        redirect(base_url('login'));
    }

    if (!$dados['flCliente']) {
        redirect(base_url('admin'));
    }
}
