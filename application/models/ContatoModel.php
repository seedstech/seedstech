<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Contato
 *
 * @author Helder dos Santos
 */
class ContatoModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function adicionar($contato) {
        return $this->db->insert("contato", $contato);
    }

    public function buscarNaoRespondidas() {
        $this->db->select('*');
        $this->db->where('flRespondida', 'false');
        return $this->db->get('contato')->result();
    }

    public function buscarRespondidas() {
        $this->db->select('*');
        $this->db->where('flRespondida', 'true');
        return $this->db->get('contato')->result();
    }

    /**
     * Busca o contato pelo id
     * @param type $idContato
     * @return type
     */
    public function buscarPorId($idContato) {
        $this->db->select('*');
        $this->db->where('idContato', $idContato);
        return $this->db->get('contato')->result();
    }

    public function excluir($idContato) {
        //$tables = array('table1', 'table2', 'table3');
        $this->db->where('idContato', $idContato);
        return $this->db->delete('contato');
    }

}
