<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of TarefaModel
 *
 * @author Heldos dos Santos
 */
class TarefaModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function adicionar($tarefa) {
        return $this->db->insert('tarefa', $tarefa);
    }

    public function buscarTodas($idProjeto) {
        $this->db->select('*');
        $this->db->where('idProjeto', $idProjeto);
        return $this->db->get('tarefa')->result();
    }

    public function excluir($idTarefa) {
        $this->db->where('idTarefa', $idTarefa);
        return $this->db->delete('tarefa');
    }

    public function finalizar($idTarefa) {
        $this->db->where('idTarefa', $idTarefa);
        return $this->db->update('tarefa', array('flFinalizada' => 1));
    }

}
