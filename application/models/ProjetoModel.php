<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of ProjetoModel
 *
 * @author Heldos dos Santos
 */
class ProjetoModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function adicionar($projeto) {
        return $this->db->insert("projeto", $projeto);
    }

    public function buscarTodos($idCliente) {
        $this->db->select('*');
        $this->db->where('idCliente', $idCliente);
        return $this->db->get('projeto')->result();
    }

    /**
     * Busca todos os projetos do cliente contando as 
     * tarefas de cada projeto e a quantidade que está finalizada
     * @param type $idCliente
     * @return type
     */
    public function buscarPorCliente($idCliente) {
        
        $this->db->select('*');
        $this->db->select('(select count(idTarefa) from tarefa where tarefa.idProjeto = projeto.idProjeto) as quantidadeDeTarefas ');
        $this->db->select('(select count(flFinalizada) from tarefa where flFinalizada = 1 and tarefa.idProjeto = projeto.idProjeto) as tarefasFinalizadas');
        $this->db->where('projeto.idCliente',$idCliente);

        return $this->db->get('projeto')->result();
    }

    public function buscarPorId($idProjeto) {
        $this->db->select('*');
        $this->db->where('idProjeto', $idProjeto);
        return $this->db->get('projeto')->result();
    }
    
    public function excluir($idProjeto){
        $this->db->where('idProjeto', $idProjeto);
        return $this->db->delete('projeto');
    }

}
