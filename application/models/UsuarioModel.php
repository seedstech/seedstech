<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of UsuarioModel
 *
 * @author Helder dos Santos
 */
class UsuarioModel extends CI_Model{
    
    public function adicionar($usuario){
        return $this->db->insert("usuario", $usuario);
    }
    
    public function alterar(){
        
    }
    
    public function excluir($id){
        $this->db->where('idUsuario',$id);
        return $this->db->delete('usuario');
    }
    
    public function listarTodos($isClientes = false){
        $this->db->select('*');
        if($isClientes){
            $this->db->where('flCliente', '1');
        }else{
            $this->db->where('flCliente', '0');
        }        
        return $this->db->get('usuario')->result();
    }
    
    public function buscarPorUsuario($usuario){
        $this->db->select('*');
        $this->db->where('usuario',$usuario);
        return $this->db->get('usuario')->result();
    }
    
    public function buscarPorId($idUsuario){
        $this->db->select('*');
        $this->db->where('idUsuario', $idUsuario);
        return $this->db->get('usuario')->result();
    }
    
}
