<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Seeds Tech - Erro 404</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/bootstrap-4.0.0-beta.1.css'); ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/animate.css'); ?>">

        <link rel="shortcut icon" href="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>" type="img/x-icon">
        <style>
            #cod-erro{
                font-size: 45pt;
                font-weight: bold;
            }
            #msg-erro{
                font-size: 35pt;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center animated fadeInUp">
                    <div id="cod-erro">404</div>
                    <div id="msg-erro">Página não encontrada!</div>
                    <a href="<?= base_url() ?>" class="btn btn-success btn-lg">Início</a>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url('assets/templateInicio/jquery-3.2.1.slim.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/bootstrap.min.js'); ?>"></script>
    </body>
</html>
