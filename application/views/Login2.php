<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/bootstrap-4.0.0-beta.1.css'); ?>" type="text/css">
        
        <title>Seeds Tech</title>
        
        <link rel="shortcut icon" href="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>" type="img/x-icon">
        
        <style>
            body {
                background: #2a313f;
                background: -moz-linear-gradient(left, #2a313f 1%, #62738e 49%, #2a313f 100%);
                background: -webkit-linear-gradient(left, #2a313f 1%, #62738e 49%, #2a313f 100%);
                background: linear-gradient(to right, #2a313f 1%, #62738e 49%, #2a313f 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2a313f', endColorstr='#2a313f', GradientType=1);
            }


            #conteudo-login{
                -webkit-box-shadow: 3px 7px 24px -8px rgba(0,0,0,0.75);
                -moz-box-shadow: 3px 7px 24px -8px rgba(0,0,0,0.75);
                box-shadow: 3px 7px 24px -8px rgba(0,0,0,0.75);
            }

            #btn-entrar{
                color: #fff;
                text-shadow: 0px 0px 7px rgba(150, 150, 150, 1);
                font-weight: bold;
            }

            #btn-entrar:hover{
                -webkit-filter:grayscale(25%);

            }
        </style>
    </head>

    <body>


        <div class="my-4 py-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"> </div>
                    <div class="col-md-4">
                        <img class="img-fluid" src="<?php echo base_url('assets/templateInicio/img/logo450x150.png'); ?>"> </div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="container">
                <div class="row"> </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4"> </div>
                    <div  class="col-md-4">
                        <div id="conteudo-login" class="card text-dark py-3 border border-dark">
                            <div class="col-md-12" style="background-color: rgb(132, 194, 37); position: absolute; top: 0; height: 15px;"> </div>
                            <div class="card-body">
                                <h1 class="mb-4 text-center">Login</h1>
                                <form action="<?php echo base_url('login/entrar'); ?>" method="post" id="form-login">

                                    <div class="form-group has-error"> <label>Usuário</label>
                                        <input id="usuario" name="usuario" type="textl" class="form-control form-control-lg form" autofocus="">
                                         <?php echo form_error('usuario', "<div class='text-danger'>", "</div>"); ?>
                                    </div>
                                    
                                    <div class="form-group"> <label>Senha</label>
                                        <input id="senha" name="senha" type="password" class="form-control form-control-lg"> 
                                        <?php echo form_error('senha', "<div class='text-danger'>", "</div>"); ?>
                                        
                                        <?php echo isset($erro)? "<div class='text-danger'>".$erro."</div>" : "";?>
                                    </div>
                                    <button type="submit" class="btn btn-block btn-lg" style="background-color:rgb(132, 194, 37);" id="btn-entrar">Entrar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="my-4 py-0 text-center">
            <div class="container">
                    <a class="text-white" href="<?php echo base_url();?>">Inicio</a>
            </div>
        </div>
        
        <script src="<?php echo base_url('assets/templateInicio/jquery-3.2.1.slim.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/jquery-3.2.1.min.js'); ?>"></script>

    </body>

</html>

