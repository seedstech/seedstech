<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/bootstrap-4.0.0-beta.1.css'); ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/estilo-apresentacao.css'); ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templateInicio/animate.css'); ?>"> </head>

    
    <title>Seeds Tech</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>" type="img/x-icon">
    
    
    
    <!-- arquivos para exibição de alerta personalizados-->
    <script src="<?php echo base_url("assets/alertas/sweetalert.min.js"); ?>" type="text/javascript"></script>
    <link href="<?php echo base_url("assets/alertas/sweetalert.css"); ?>" rel="stylesheet" type="text/css"/>

    <body>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark p-3" id="menu-apresentacao">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>"> </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
                    <ul class="navbar-nav text-center">
                        <li class="nav-item">
                            <a class="nav-link" href="#" target=""> Início</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#quem-somos"> Sobre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#time">Time</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#produtos">Produtos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contato">Contato</a>
                        </li>
                    </ul>
                    <a href="<?php echo base_url('login'); ?>" class="btn navbar-btn ml-2 btn-default btn-dark border border-warning text-warning"><i class="fa fa-user fa-user"></i>&nbsp;Entrar</a>
                </div>
            </div>
        </nav>
        <div class="py-5 text-center opaque-overlay" style="background-image: url('<?php echo base_url('assets/templateInicio/img/fundo-escuro.jpg'); ?>');" id="apresentacao-img">
            <div class="container py-5">
                <div class="row">
                    <div class="col-12 text-white">
                        <h1 class="display-3 mb-4">
                            <img class="animated bounceInDown img-fluid" alt="Responsive image" src="<?php echo base_url('assets/templateInicio/img/logo450x150.png'); ?>"> </h1>
                        <p class="lead mb-6" id="texto-inicio"> Precisando de sistema web?
                            <br>A gente faz para você!</p>
                        <a id="btn-contato" href="#quem-somos" class="btn btn-lg mx-1 ">Veja quem somos</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="quem-somos" class="py-5 bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center display-4 text-success">Conheça-nos melhor!</h1>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12 animated">
                        <p class="lead">A Seeds Tech é uma fábrica de desenvolvimento de software capaz de produzir sistemas que atenda às metas estratégicas da organização. Utilizando as melhores ferramentas e metodologias do mercado e uma equipe especialista em desenvolver sistemas
                            alinhados com o negócio da organização, de maneira que adeque melhor o sistema aos processos internos de sua empresa.</p>
                        <div class="row text-left mt-5">

                            <div class="col-md-4 my-3 bg-secondary py-4 animado">
                                <div class="row mb-3">
                                    <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-neuter text-white"></i></div>
                                    <div class="align-self-center col-10">
                                        <h5 class="text-secondary"><b class="text-white">Missão</b></h5>
                                    </div>
                                </div>
                                <p class="text-justify text-white">Desenvolver sistemas alinhados às metas estratégicas da organização cliente. </p>
                            </div>


                            <div class="col-md-4 my-3 bg-success py-4 animado">
                                <div class="row mb-3">
                                    <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-neuter text-white"></i></div>
                                    <div class="align-self-center col-10">
                                        <h5 class="text-secondary"><b class="text-white">Visão</b></h5>
                                    </div>
                                </div>
                                <p class="text-justify text-white">Tornar-nos uma empresa líder de mercado, no desenvolvimento de aplicações que esteja alinhado às metas estratégicas da empresa. Oferecendo ao cliente um serviço que proporcione crescimento econômico e destaque no mercado.</p>
                            </div>


                            <div class="col-md-4 my-3 bg-danger py-4 animado">
                                <div class="row mb-3">
                                    <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-neuter text-white"></i></div>
                                    <div class="align-self-center col-10">
                                        <h5 class="text-secondary"><b class="text-white">Valores</b></h5>
                                    </div>
                                </div>
                                <p> </p>
                                <ul class="text-white">
                                    <li>Inovação</li>
                                    <li>Respeito</li>
                                    <li>Comprometimento</li>
                                    <li>Ética</li>
                                    <li>Responsabilidade</li>
                                    <li>Qualidade</li>
                                    <li>Excelência</li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="time" class="py-5 text-center bg-success">
            <div class="container" id="efeito-time">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center display-4 text-light">Estes são os nossos especialistas</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/herundina.jpg'); ?>">
                        <p class="text-white"><b>Herundina Ferreira</b>
                            <br>Gerente de Projetos</p>
                    </div>
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/fernando.jpg'); ?>">
                        <p class="text-white"><b>Fernando Macêdo</b>
                            <br>Analista de Teste</p>
                    </div>
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/helder.jpg'); ?>">
                        <p class="text-white"><b>Helder dos Santos</b>
                            <br>Arquiteto de Software</p>
                    </div>
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/bruno.jpg'); ?>">
                        <p class="text-white"><b>Erverson Bruno</b>
                            <br>Analista de Requisitos</p>
                    </div>
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/lucas.jpg'); ?>">
                        <p class="text-white"><b>Lucas Xenofonte</b>
                            <br>Designer de Interface</p>
                    </div>
                    <div class="col-md-4 p-4">
                        <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/img/masyaf.jpg'); ?>">
                        <p class="text-white"><b>Masyaf</b>
                            <br>Programador</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="produtos" class="py-5 text-center text-white opaque-overlay gradient-overlay" style="background-image: url('<?php echo base_url('assets/templateInicio/img/negocio.jpg'); ?>');">
            <div class="container">
                <div class="row py-4 my-0">
                    <div class="col-md-12 animated">
                        <h1 class="text-center display-4 text-light">Veja só os nossos produtos</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="p-4 col-md-3"> <i class="d-block fa fa-3x fa-pie-chart"></i>
                        <h2 class="my-3">Improve</h2>
                        <p>Based on fluid design principles. Works with any screen resolution</p>
                    </div>
                    <div class="col-md-3 p-4"> <i class="d-block fa fa-3x fa-th"></i>
                        <h2 class="my-3">Scale</h2>
                        <p>Work simultaneously on different panels. Share the work with teammates.</p>
                    </div>
                    <div class="col-md-3 p-4 text-center"> <i class="d-block fa fa-3x fa-globe"></i>
                        <h2 class="my-3">Spread</h2>
                        <p>Help us spreading the word. Tell your friends with just one-click</p>
                    </div>
                    <div class="col-md-3 p-4"> <i class="d-block fa fa-3x fa-money"></i>
                        <h2 class="my-3">Make money</h2>
                        <p>Choose settings depending on the criteria you value the most</p>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div id="contato" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="display-4 text-dark" style="width: 100%;">Entre em contato conosco </h1>
                    </div>
                    <div id="icone-enviarr" class="col-md-6"> 
                        <!--<i class="fa fa-send text-default text-primary"></i> -->

                        <div class="contact-info-box address clearfix">
                            <h3><i class="fa mr-3 fa-map-marker text-success"></i>Endereço:</h3>
                            <span>Juazeiro do Norte - CE</span>
                        </div>
                        <div class="contact-info-box email clearfix">
                            <h3><i class="fa mr-3 fa-envelope-o text-success"></i>mail:</h3>
                            <span>contato@seedstech.com.br</span>
                        </div>
                        <div class="contact-info-box hours clearfix">
                            <h3><i class="fa mr-3 fa-clock-o text-success"></i>Horário:</h3>
                            <span><strong>Adivinha?</strong> Nossos servidores estão sempre de portas abertas para receber seu contato!</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form id="form-contato" method="post" action="">
                            <div class="form-group"> <label for="nome">Nome*</label>
                                <input type="text" class="form-control border border-dark" id="nome" name="nome"> </div>
                            <div class="form-group"> <label for="email">E-mail</label>
                                <input type="email" class="form-control border border-dark" id="email" name="email"> </div>
                            <div class="form-group"> <label for="telefone">Telefone*</label>
                                <input type="text" class="form-control border border-dark" id="telefone" name="telefone"> </div>
                            <div class="form-group"> <label for="Textarea">Mensagem*</label> <textarea class="form-control border border-dark" rows="3" id="descricao" name="descricao"></textarea> </div>

                            <div class="form-group">
                                <input id="receberEmail" name="receberEmail" type="checkbox" checked> <label for="receberEmail">Deseja receber E-mails informativos?</label>
                            </div>

                            <!--<button type="submit" class="btn pull-right btn-success">Enviar</button>-->
                            <div class="grupo-direita">
                                <button type="submit" class="btn btn-success" id="enviar">Enviar</button>
                                <img class="carregando-form"  src="<?php echo base_url('assets/img/carregar.gif'); ?>">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="voltar-topo" title="Voltar ao Topo"> <i class="fa fa-angle-double-up"></i> </div>
        <div class="text-white bg-dark">
            <div class="container">
                <div class="row">
                    <div class="p-4 col-md-6">
                        <h2 class="mb-4 text-white">Mapa do Site</h2>
                        <ul class="list-unstyled">
                            <a href="#" class="text-white">Início</a>
                            <br>
                            <a href="#quem-somos" class="text-white">Sobre</a>
                            <br>
                            <a href="#time" class="text-white">Time</a>
                            <br>
                            <a href="#produtos" class="text-white">Produtos</a>
                            <br>
                            <a href="#contato" class="text-white">Contato</a>
                        </ul>
                    </div>
                    <div class="p-4 col-md-6">
                        <h2 class="mb-4 text-white">Contato</h2>
                        <p>
                            <a href="" class="text-white"><i class="fa d-inline mr-3 fa-envelope-o text-success"></i>contato@seedstech.com.br</a>
                        </p>
                        <p>
                            <a href="" class="text-white" target="_blank"><i class="fa d-inline mr-3 fa-map-marker text-success"></i>Juazeiro do Norte, CE</a>
                        </p>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 mt-3">
                        <p class="text-center text-white">&copy; Copyright 2017 Seeds Tech - All rights reserved. </p>
                    </div>
                </div>


            </div>
        </div>
        <script src="<?php echo base_url('assets/templateInicio/jquery-3.2.1.slim.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/jquery-3.2.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/templateInicio/efeitos-gui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/util/Requisicoes.js"); ?>"></script>

        <script type="text/javascript">
            baseUrl = "<?php echo base_url(); ?>";
        </script>

    </body>

</html>
