<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $titulo ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("cliente"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-comments-o"></i><?= $titulo ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <br>
        <br>
        <br>
        <br>
        <br>
        
        <div class="row">
            <div class="col-md-12">
                <h1>
                    Em breve você poderá abrir um chamado a partir desta página.
                </h1>
            </div>
        </div>