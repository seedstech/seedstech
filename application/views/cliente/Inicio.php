<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Andamento dos Projetos
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("cliente"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> Andamento dos Projetos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <table id="tableclientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Cod.</th>
                    <th>Projeto</th>
                    <th>Data de Início</th>
                    <th>Data prevista término</th>
                    <th>Andamento do Projeto</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 0;
                $porcentagem = 0;
                foreach ($projetos as $projeto):
                    $i++;                    
                    $resultado = 0;
                    
                    if($projeto->quantidadeDeTarefas > 0){
                    	$resultado = $projeto->tarefasFinalizadas * 100 / $projeto->quantidadeDeTarefas;
                    }                   
                    
                    $porcentagem = number_format($resultado, 2, '.', ',');
                    ?>

                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $projeto->nome ?></td>
                        <td><?= date('d/m/Y', strtotime($projeto->dataInicio)) ?></td>
                        <td><?= date('d/m/Y', strtotime($projeto->dataPrevistaFim)) ?></td>
                        <td>
                            <div class="progress" style="border:1px solid #cccccc; background-color: #cccccc">
                                <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar"
                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?= $porcentagem ?>%">
                                    <?= $porcentagem ?> % Concluído
                                </div>
                            </div>                        
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
