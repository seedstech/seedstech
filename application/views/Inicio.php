<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">

        <title>Seeds Tech - Soluções em Informática</title>
        <link rel="icon" href="<?php echo base_url('assest/icone32x32.png'); ?>" type="image/png">
        <link rel="shortcut icon" href="<?php echo base_url('assets/icone32x32.ico'); ?>" type="img/x-icon">

        <!-- 
        <link rel="icon" href="favicon.png" type="image/png">
        <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">
        -->

        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/font-awesome.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/responsive.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet" type="text/css">

        <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.1.8.3.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-scrolltofixed.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.easing.1.3.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.isotope.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/wow.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/classie.js"); ?>"></script>
        
        
        <script type="text/javascript" src="<?php echo base_url("assets/js/util/Requisicoes.js"); ?>"></script>
        
        <!-- arquivos para exibição de alerta personalizados-->
        <script src="<?php echo base_url("assets/alertas/sweetalert.min.js"); ?>" type="text/javascript"></script>
        <link href="<?php echo base_url("assets/alertas/sweetalert.css"); ?>" rel="stylesheet" type="text/css"/>
        
        
        <!-- configurações de constantes do arquivo assets/util/requisicoes.js-->
        <script type="text/javascript">
            baseUrl = "<?php echo base_url();?>";
        </script>

    </head>
    <body>
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>Menu
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll " href="index.php"><img id="LogoNavBar" src="<?php echo base_url("assets/img/logonavbar.png"); ?>" alt=""> Início</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" style="font-size: 16px; padding-right: 40px; padding-top: 15px;">
                        <li><a href="#service">Serviços</a></li>
                        <!--<li><a href="#Portfolio">Portfólio</a></li>-->
                        <li><a href="#client">Clientes</a></li>
                        <li><a href="#team">Equipe</a></li>
                        <li><a href="#contact">Contato</a></li>
                        <li><a href="<?php echo base_url("login"); ?>">Área interna</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>


        <!--main-nav-end-->
        <header class="header" id="header"><!--header-start-->
            <div class="container">
                <div><h1 id="Seeds" class="animated fadeInDown delay-06s"><img id="LogoSeeds" src="<?php echo base_url("assets/img/logoseeds.png"); ?>" alt=""></h1></div>
                <hr>

                <div>
                    <div class="col-lg-6">
                        <h4  id="FraseInicio" class="we-create animated fadeInUp delay-06s">Precisa de soluções para você ou sua empresa? </h4>
                    </div>
                    <div class="col-lg-6">
                        <h4  id="FraseInicio" class="we-create animated fadeInUp delay-06s">Nós somos nerds criadores de sistemas...</h4>
                    </div>
                </div>

                <a id="seta" class="fadeInUp delay-1s link fa fa-angle-double-down " href="#service"></a>
            </div>
        </header><!--header-end-->





        <section class="main-section" ><!--main-section-start-->
            <div class="container">
                <h2 id="service">Serviços</h2>
                <h6>Oferecemos um serviço excepcional para melhor divugação de seu produto!</h6>
                <div class="row">
                    <div class="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
                        <div class="service-list">
                            <div class="service-list-col1">
                                <i class="fa-check-square-o"></i>
                            </div>
                            <div class="service-list-col2">
                                <h3>Marca &amp; identidade</h3>
                                <p>Qualidade igual não há!</p>
                            </div>
                        </div>
                        <div class="service-list">
                            <div class="service-list-col1">
                                <i class="fa-expand"></i>
                            </div>
                            <div class="service-list-col2">
                                <h3>Responsividade</h3>
                                <p>Não importa em qual dispositivo você acesse seu site, nós adaptaremos!</p>
                            </div>
                        </div>
                        <div class="service-list">
                            <div class="service-list-col1">
                                <i class="fa-apple"></i>
                            </div>
                            <div class="service-list-col2">
                                <h3>Design inovador</h3>
                                <p>Cores, formas e efeitos são responsáveis por dar um toque especial ao seu site!</p>
                            </div>
                        </div>
                        <div class="service-list">
                            <div class="service-list-col1">
                                <i class="fa-medkit"></i>
                            </div>
                            <div class="service-list-col2">
                                <h3>Suporte 24/7</h3>
                                <p>Prestamos socorro 24 horas por dia, 7 dias por semana!</p>
                            </div>
                        </div>
                    </div>
                    <figure class="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
                        <img id="reponsa-capa.png" src="<?php echo base_url('assets/img/reponsa-capa.png'); ?>" alt="">
                    </figure>

                </div>
            </div>
        </section><!--main-section-end-->



        <section class="main-section alabaster"><!--main-section alabaster-start-->
            <div class="container">
                <div class="row">
                    <figure class="col-lg-5 col-sm-4 wow fadeInLeft">

                        <img  src="<?php echo base_url('assets/img/Sua empresa ainda não tem site.png'); ?>" alt="">
                        <br>
                    </figure>
                    <div class="col-lg-7 col-sm-8 featured-work">
                        <h2 id="h2Especificacoes">Vantagens</h2>
                        <P  class="padding-b">Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit.</P>
                        <div class="featured-box">
                            <div class="featured-box-col1 wow fadeInRight delay-02s">
                                <i class="fa-envelope"></i>
                            </div>	
                            <div class="featured-box-col2 wow fadeInRight delay-02s">
                                <h3>magic of theme development</h3>
                                <p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
                            </div>    
                        </div>
                        <div class="featured-box">
                            <div class="featured-box-col1 wow fadeInRight delay-04s">
                                <i class="fa-at"></i>
                            </div>	
                            <div class="featured-box-col2 wow fadeInRight delay-04s">
                                <h3>neatly packaged</h3>
                                <p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
                            </div>    
                        </div>
                        <div class="featured-box">
                            <div class="featured-box-col1 wow fadeInRight delay-06s">
                                <i class="fa fa-line-chart"></i>
                            </div>	
                            <div class="featured-box-col2 wow fadeInRight delay-06s">
                                <h3>SEO optimized</h3>
                                <p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. </p>
                            </div>    
                        </div>
                        <a class="Learn-More" href="#">Learn More</a>
                    </div>
                </div>
            </div>
        </section><!--main-section alabaster-end-->


        <!--main-section-start-->
        <!--<section class="main-section paddind" >
            <div class="container">
                <h2 id="Portfolio">Portfolio</h2>
                <h6 >Fresh portfolio of designs that will keep you wanting more.</h6>
            </div>
            <div class="portfolioContainer wow fadeInUp delay-04s">
                <div class=" Portfolio-box printdesign">
                    <a href="http://www.j-artesxenofonte.com.br/"><img src="img/J Artes Xenofonte.png" alt=""></a>	
                    <h3>J Artes Xenofonte</h3>
                    <p>Criação e restauração de Móveis</p>
                </div>
            </div>
        </section>-->
        <!--main-section-end-->


        <section id="client" class="main-section client-part" ><!--main-section client-part-start-->
            <div  class="container">
                <b class="quote-right wow fadeInDown delay-03"><i class="fa-quote-right"></i></b>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="client-part-haead wow fadeInDown delay-05"> "... o objetivo de usar os sistemas de informação é
                            a criação de um ambiente empresarial em que as informações sejam confiáveis e possam fluir na estrutura organizacional"</p>
                    </div>
                </div>
                <ul class="client wow fadeIn delay-05s">
                    <li><a href="#">
                            <h3>Batista</h3>
                            <span>(2004, p. 39 apud Bazzotti on-line, p. 6)</span>
                        </a></li>
                </ul>
            </div>
        </section><!--main-section client-part-end-->
        <div class="c-logo-part"><!--c-logo-part-start-->
            <div class="container">
                <ul>
                    <li><a href="#"><img src="<?php echo base_url('assets/img/c-liogo1.png'); ?>" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url('assets/img/c-liogo2.png'); ?>" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url('assets/img/c-liogo3.png'); ?>" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url('assets/img/c-liogo4.png'); ?>" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo base_url('assets/img/c-liogo5.png'); ?>" alt=""></a></li>
                </ul>
            </div>
        </div><!--c-logo-part-end-->
        <section class="main-section team" ><!--main-section team-start-->
            <div class="container">
                <h2 id="team">Equipe</h2>
                <h6>A Seeds Tech possui uma equipe nerd pronta para vos atender...</h6>
                <div class="team-leader-block clearfix">
                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-03s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/team-leader-pic1.jpg'); ?>" alt="">
                            <ul>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-03s">Fernando Macedo</h3>
                        <span class="wow fadeInDown delay-03s">Gerente de Projetos</span>
                    </div>
                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-03s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/team-leader-pic1.jpg'); ?>" alt="">
                            <ul>
                                <li><a href="#" class="fa-twitter"></a></li>
                                <li><a href="#" class="fa-facebook"></a></li>
                                <li><a href="#" class="fa-pinterest"></a></li>
                                <li><a href="#" class="fa-google-plus"></a></li>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-03s">Helder Santos</h3>
                        <span class="wow fadeInDown delay-03s">Analista de Requisitos</span>
                    </div>
                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-03s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/team-leader-pic1.jpg'); ?>" alt="">
                            <ul>
                                <li><a href="#" class="fa-twitter"></a></li>
                                <li><a href="#" class="fa-facebook"></a></li>
                                <li><a href="#" class="fa-pinterest"></a></li>
                                <li><a href="#" class="fa-google-plus"></a></li>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-03s">Erverson Bruno</h3>
                        <span class="wow fadeInDown delay-03s">Projetista de Sistemas</span>
                    </div>
                    <div class="team-leader-box">
                        <div class="team-leader  wow fadeInDown delay-06s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/team-leader-pic2.jpg'); ?>" alt="">
                            <ul>
                                <li><a href="#" class="fa-twitter"></a></li>
                                <li><a href="#" class="fa-facebook"></a></li>
                                <li><a href="#" class="fa-pinterest"></a></li>
                                <li><a href="#" class="fa-google-plus"></a></li>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-06s">Gesualdo Matias (Masyaf) </h3>
                        <span class="wow fadeInDown delay-06s">Desenvolvedor Back-end</span>
                    </div>
                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-09s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/lucasxenofonte.jpg'); ?>" alt="">
                            <ul>
                                <li><a href="https://www.facebook.com/lucas.xenofonte.lobo" class="fa-facebook"></a></li>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-09s">Lucas Xenofonte</h3>
                        <span class="wow fadeInDown delay-09s">Desenvolvedor Front-end</span>
                    </div>


                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-03s"> 
                            <div class="team-leader-shadow"><a href="#"></a></div>
                            <img src="<?php echo base_url('assets/img/team-leader-pic1.jpg'); ?>" alt="">
                            <ul>
                                <li><a href="#" class="fa-twitter"></a></li>
                                <li><a href="#" class="fa-facebook"></a></li>
                                <li><a href="#" class="fa-pinterest"></a></li>
                                <li><a href="#" class="fa-google-plus"></a></li>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-03s">Herondina</h3>
                        <span class="wow fadeInDown delay-03s">Analista de Testes</span>
                    </div>
                </div>
            </div>
        </section><!--main-section team-end-->



        <section class="business-talking"><!--business-talking-start-->
            <div class="container">
                <h2>Vamos falar sobre negócios?!</h2>
            </div>
        </section><!--business-talking-end-->


        <div  class="container" style="margin-bottom: 5px;">
            <section class="main-section contact" >
                <h2 id="contato">Contato</h2>
                <div class="row col-lg-6 col-sm-7" id="infocontact">
                    <div class=" wow fadeInLeft">
                        <div class="contact-info-box address clearfix">
                            <h3><i class=" icon-map-marker"></i>Endereço:</h3>
                            <span>Juazeiro do Norte - CE</span>
                        </div>
                        <!--<div class="contact-info-box phone clearfix">
                            <h3><i class="fa-phone"></i>Phone:</h3>
                            <span>1-800-BOO-YAHH</span>
                        </div>-->
                        <div class="contact-info-box email clearfix">
                            <h3><i class="fa-pencil"></i>email:</h3>
                            <span>contato@seedstech.com.br</span>
                        </div>
                        <div class="contact-info-box hours clearfix">
                            <h3><i class="fa-clock-o"></i>Horário:</h3>
                            <span><strong>Advinha?</strong> Nossos servidores estão de portas abertas para receber seu contato!</span>
                        </div>
                        <!-- Mídias sociais da empresa
                        <ul class="social-link">
                            <li class="twitter"><a href="#"><i class="fa-twitter"></i></a></li>
                            <li class="facebook"><a href="#"><i class="fa-facebook"></i></a></li>
                            <li class="pinterest"><a href="#"><i class="fa-pinterest"></i></a></li>
                            <li class="gplus"><a href="#"><i class="fa-google-plus"></i></a></li>
                            <li class="dribbble"><a href="#"><i class="fa-dribbble"></i></a></li>
                        </ul>
                        -->
                    </div>

                </div>
                <div class="row col-lg-6 col-sm-7" id="contact">
                    <div class=" wow fadeInLeft">

                        <form id="form-contato" action="" method="post">


                            <div class="form-group">
                                <label for="">Nome Completo:*</label>
                                <input id="nome" type="text" name="nome" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Telefone:</label>
                                <input id="telefone" type="text" name="telefone" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email:</label>
                                <input id="email" type="text" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Descrição:*</label>
                                <textarea id="descricao" name="descricao" class="form-control"></textarea><br>
                            </div>
                            <div class="checkbox">
                                <label><input id="receberEmail" name="receberEmail" type="checkbox" checked> Deseja receber E-mails informativos?</label>
                                
                            </div>
                            <div>
                                <center><button type="submit" class="btn btn-default" >Submit</button></center>
                            </div>

                        </form>

                    </div>

                </div>
            </section>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="<?php echo base_url('assets/img/logoseeds.png'); ?>" alt=""></a></div>
                <div class="credits">
                    <!-- 
                        All the links in the footer should remain intact. 
                        You can delete the links only if you purchased the pro version.
                        Licensing information: https://bootstrapmade.com/license/
                        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Knight
                    -->
                    <a href="index.php">Seeds Tech</a> - Soluções em Informática
                </div>
            </div>
        </footer>


        <script type="text/javascript">
            $(document).ready(function (e) {
                $('#test').scrollToFixed();
                $('.res-nav_click').click(function () {
                    $('.main-nav').slideToggle();
                    return false

                });

            });
        </script>

        <script>
            wow = new WOW(
                    {
                        animateClass: 'animated',
                        offset: 100
                    }
            );
            wow.init();
        </script>


        <script type="text/javascript">
            $(window).load(function () {

                $('.main-nav li a, .servicelink').bind('click', function (event) {
                    var $anchor = $(this);

                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top - 102
                    }, 1500, 'easeInOutExpo');
                    /*
                     if you don't want to use the easing effects:
                     $('html, body').stop().animate({
                     scrollTop: $($anchor.attr('href')).offset().top
                     }, 1000);
                     */
                    if ($(window).width() < 768) {
                        $('.main-nav').hide();
                    }
                    event.preventDefault();
                });
            })
        </script>

        <script type="text/javascript">

            $(window).load(function () {


                var $container = $('.portfolioContainer'),
                        $body = $('body'),
                        colW = 375,
                        columns = null;


                $container.isotope({
                    // disable window resizing
                    resizable: true,
                    masonry: {
                        columnWidth: colW
                    }
                });

                $(window).smartresize(function () {
                    // check if columns has changed
                    var currentColumns = Math.floor(($body.width() - 30) / colW);
                    if (currentColumns !== columns) {
                        // set new column count
                        columns = currentColumns;
                        // apply width to container manually, then trigger relayout
                        $container.width(columns * colW)
                                .isotope('reLayout');
                    }

                }).smartresize(); // trigger resize to set container width
                $('.portfolioFilter a').click(function () {
                    $('.portfolioFilter .current').removeClass('current');
                    $(this).addClass('current');

                    var selector = $(this).attr('data-filter');
                    $container.isotope({
                        filter: selector,
                    });
                    return false;
                });

            });

        </script>

    </body>
</html>