<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Caixa de Mensagens
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> Caixa de Mensagens</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <table id="tablemensagens" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th>Descrição</th>
                    <th>Receber E-mail</th>
                    <th>Responder</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($mensagens as $msg): ?>

                    <tr>
                        <td><?php echo $msg->nome; ?></td>
                        <td><?php echo $msg->telefone; ?></td>
                        <td><?php echo $msg->email; ?></td>
                        <td><?php echo $msg->descricao; ?></td>
                        <td><?php echo $msg->flReceberEmail == true ? 'Sim' : 'Não'; ?></td>



                        <td>
                            <a href="<?= base_url('contato/formemail/' . $msg->idContato) ?>">
                                <span class="fa fa-send text-blue" title="Responder mensagem"></span>
                            </a>
                        </td>

                        <td>
                            <a href="<?= base_url('contato/abrir/' . $msg->idContato) ?>">
                                <span class="fa fa-eye text-success" title="Ver Respostas"></span>
                            </a>
                            <a href="javascript:void(0)" onclick="excluirContato('<?= base_url('contato/excluir') ?>', '<?= $msg->idContato ?>', '<?= $msg->nome ?>')">
                                <span class="fa fa-remove text-red" title="Excluir Mensagem"></span>
                            </a>
                        </td>
                    </tr>

                <?php endforeach; ?>

            </tbody>
        </table>

        <script>


        </script>

        <script>
            /**
             * Função para excluir mensagem de solicitação de contato
             * @param {type} url
             * @param {type} idContato
             * @param {type} nome
             * @returns {undefined}
             */
            function excluirContato(url, idContato, nome) {
                $c = jQuery.noConflict();
                swal({
                    title: "Atenção",
                    text: "Deseja realmente excluir esta mensagem do cliente " + nome + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Excluir",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function(isConfirm) {
                    if (isConfirm) {
                        $c.post(url, {contato: idContato}, function(data) {
                            var tipoAlerta = "";
                            if (data.status == 1) {
                                tipoAlerta = "success";
                            } else {
                                tipoAlerta = "error";
                            }

                            swal({
                                title: "",
                                text: data.msg,
                                type: tipoAlerta,
                                showCancelButton: false,
                                confirmButtonText: "Ok",
                                closeOnConfirm: false,
                                closeOnCancel: true
                            }, function(isConfirm) {
                                if (isConfirm) {
                                    window.location.href = "<?= base_url('admin') ?>";
                                }
                            });
                        }, 'json');
                    }
                });

            }
        </script>
