

<!-- Aqui começa o cabeçalho com barra de navegação da página-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Usuários
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-cogs"></i> Configurações (Usuários)</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
        <!-- Aqui começa de fato o conteúdo da página-->
        <section class="content">

            <table id="tableusuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Email</th>
                        <th>Usuário</th>
                        <th>Senha</th>
                        <th>Situação</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Email</th>
                        <th>Usuário</th>
                        <th>Senha</th>
                        <th>Situação</th>
                        <th>Opções</th>
                    </tr>
                </tfoot>
                <tbody>

                    <?php foreach ($usuarios as $usuario) : ?>
                        <tr>
                            <td><?php echo $usuario->nome; ?></td>
                            <td><?php echo $usuario->telefone; ?></td>
                            <td><?php echo $usuario->email; ?></td>
                            <td><?php echo $usuario->usuario; ?></td>
                            <td>****</td>

                            <?php if ($usuario->flAtivo) { ?>
                                <td>Ativo</td>
                            <?php } else { ?>
                                <td>Inativo</td>
                            <?php } ?>

                            <td class="text-center">
                                <?php if($this->session->userdata('idUsuario') != $usuario->idUsuario ): ?>
                                <a href="<?php echo base_url('admin/usuario/paginaadmalterar/' . $usuario->idUsuario); ?>"><span class="glyphicon glyphicon-pencil" title="Editar usuário" style="color: brown;"></span></a>
                                <a href="<?php echo base_url('admin/usuario/excluir/' . $usuario->idUsuario); ?>"><span class="glyphicon glyphicon-trash" title="Excluir usuário" style="color: black;"></span></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            <br>
            <div align="right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modaladdcliente">Cadastrar Cliente</button>
            </div>

            <!-- Modal de confirmação de exclusão de usuário da tabela-->
            <div class="modal fade" id="modaldelusuario" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Atenção!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Deseja sair?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Não!</button>
                            <a href="<?php echo base_url('login/sair'); ?>"><button type="button" class="btn btn-success" >Sim, quero sair!</button></a>
                        </div>
                    </div>
                </div>
            </div>



            <!--Modal de Cadastro de Usuário-->

            <div class="modal fade" id="modaladdcliente" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Cadastro de Usuário</h4>
                        </div>
                        <form id="formCadUsuario" action="" method="post">
                            <div class="modal-body">

                                <p>Favor preencher os campos abaixo para cadastrar novo usuário:</p>

                                <div id="campoNome" class="form-group"><!--has-error-->
                                    <input id = "nome" type="text" name="nome"class="form-control somenteLetras" placeholder="Nome" autofocus="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoTelefone" class="form-group">
                                    <input id = "telefone" type="text" name="telefone"class="form-control maskTelefone" placeholder="Telefone">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoEmail" class="form-group">
                                    <input id = "email" type="text" name="email"class="form-control" placeholder="Email">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoUsuario" class="form-group">
                                    <input id = "usuario" type="text" name="usuario" class="form-control somenteLetras" placeholder="Usuário">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoSenha" class="form-group">
                                    <input id = "senha" type="password" name="senha"class="form-control" placeholder="Senha">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoConfirmaSenha" class="form-group">
                                    <input id = "confirmaSenha" type="password" name="confirmarSenha"class="form-control" placeholder="Confirmar Senha">
                                    <span class="help-block"></span>
                                </div>


                                <p><input id="administrador" type="checkbox" name="flAdministrador"><label for="administrador">Administrador?</label></p>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                <button type="submite" class="btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--Scripts da datatable (tableusuarios)-->
            <script type="text/javascript">
                baseUrl = "<?php echo base_url(); ?>";

                $(document).ready(function () {
                    $('#tableusuarios').DataTable();
                });
            </script>

            <script type="text/javascript">
                $('#tableusuarios').DataTable({
                    scrollY: 400
                });
            </script>





            <script type="text/javascript" src="jquery.dataTables.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#tableusuarios').DataTable({
                        "language": {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            }
                        }
                    });
                });
            </script>
        </section>
    </section>
</div>