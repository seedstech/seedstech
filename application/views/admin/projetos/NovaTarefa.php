<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $titulo; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>">Inicio</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> <?php echo $titulo; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3"></div>

            <div class="col-md-6">
                <form action="<?php echo base_url('admin/tarefa/cadastrar');?>" method="post">
                    <input type="hidden" value="<?php echo $projeto[0]->idProjeto;?>" name="idProjeto">
                    
                    <div class="form-group">
                        <label for="projeto">Projeto</label>
                        <input id="projeto" value="<?php echo $projeto[0]->nome;?>" type="text" name="projeto" class="form-control" disabled="">
                    </div>
                    
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input id="nome" type="text" name="nome" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label for="dataTermino">Data Prevista Para Termino</label>
                        <input id="dataTermino" type="date" name="dataTermino" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <div class="pull-right btn-alinhado-direita">
                            <button type="submit" class="btn btn-success pull-right pa">Cadastrar</button>
                        </div>
                        <div class="pull-right btn-alinhado-direita">
                            <button type="reset" class="btn btn-warning pull-right">Cancelar</button>
                        </div>
                        <div class="pull-right btn-alinhado-direita">
                            <a href="<?php echo base_url('admin/tarefa/listaTarefas/'.$projeto[0]->idProjeto); ?>" class="btn btn-default pull-right">Voltar</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        </script>

        <style>
            .btn-alinhado-direita{
                padding-left: 10px;
            }
        </style>
