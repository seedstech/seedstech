<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <?php if (isset($sucesso)): ?>
        <?php if ($sucesso == 1): ?>
            <script>
                swal({
                    title: "",
                    text: "Tarefa cadastrada com sucesso!",
                    type: "success",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Tarefa/listaTarefas/' . $idProjeto); ?>";
                        });
            </script>
        <?php else: ?>
            <script>
                swal({
                    title: "",
                    text: "Erro ao cadastrar tarefa!",
                    type: "error",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Tarefa/listaTarefas/' . $idProjeto); ?>";
                        });
            </script>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($sucessoExcluir)): ?>
        <?php if ($sucessoExcluir == 1): ?>
            <script>
                swal({
                    title: "",
                    text: "Tarefa excluída com sucesso!",
                    type: "success",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Tarefa/listaTarefas/' . $idProjeto); ?>";
                        });
            </script>
        <?php else: ?>
            <script>
                swal({
                    title: "",
                    text: "Erro ao excluir tarefa!",
                    type: "error",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Tarefa/listaTarefas/' . $idProjeto); ?>";
                        });
            </script>
        <?php endif; ?>
    <?php endif; ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $titulo; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("cliente"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> Andamento dos Projetos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo base_url('admin/Projeto/listaProjetos/' . $idCliente); ?>" class="btn btn-primary">Voltar</a>
            </div>
        </div>
        <table id="tableclientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Tarefas</th>
                    <th>Data Prevista para termino</th>
                    <th>Finalizada</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tarefas as $tarefa): ?>
                    <tr>
                        <td><?php echo $tarefa->nome; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($tarefa->dataTermino)); ?></td>
                        <td><?php echo $tarefa->flFinalizada == true ? "Sim" : "Não"; ?></td>
                        <td>
                            <?php if ($tarefa->flFinalizada == false): ?>
                            <a class="btn btn-primary" href="javascript:void(0)" onclick="finalizar('<?= $tarefa->idTarefa ?>', '<?= $tarefa->nome ?>')">Finalizar Tarefa</a>
                            <?php else: ?>
                            <a class="btn btn-default" href="javascript:void(0)">Finalizar Tarefa</a>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="text-danger" onclick="excluir('<?= $tarefa->idTarefa ?>', '<?= $tarefa->nome ?>')"><span class="glyphicon glyphicon-trash" title="Excluir usuário" style="color: black;"></span></a>
                        </td>
                    </tr> 
                <?php endforeach; ?>

            </tbody>
        </table>

        <div align="right">
            <a href="<?php echo base_url('admin/Tarefa/novatarefa/' . $idProjeto); ?>" class="btn btn-primary">Nova Tarefa</a>
        </div>

        <script>
            function excluir(idTarefa, tarefa) {
                confirmecao = confirm('Deseja realmente excluir a tarefa ' + tarefa);

                if (confirmecao == true) {
                    window.location.href = "<?= base_url('admin/Tarefa/excluir/' . $idProjeto . '/') ?>" + idTarefa;
                }

            }
            
            function finalizar(idTarefa, tarefa){
            confirmecao = confirm('Deseja realmente Finalizar a tarefa ' + tarefa);

                if (confirmecao == true) {
                    window.location.href = "<?= base_url('admin/Tarefa/finalizar/' . $idProjeto . '/') ?>" + idTarefa;
                }
            }

        </script>