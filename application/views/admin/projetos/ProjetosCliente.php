<!-- Content Wrapper. Contains page content -->



<div class="content-wrapper">

    <?php if (isset($sucessoExcluir)): ?>
        <?php if ($sucessoExcluir == 1): ?>
            <script>
                swal({
                    title: "",
                    text: "Projeto excluído com sucesso!",
                    type: "success",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Projeto/listaProjetos/' . $cliente[0]->idUsuario); ?>";
                        });
            </script>
        <?php else: ?>
            <script>
                swal({
                    title: "",
                    text: "Erro ao excluir tarefa!",
                    type: "error",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Projeto/listaProjetos/' . $cliente[0]->idUsuario); ?>";
                        });
            </script>
        <?php endif; ?>
    <?php endif; ?>


    <?php if (isset($sucesso)): ?>
        <?php if ($sucesso == 1): ?>
            <script>
                swal({
                    title: "",
                    text: "Projeto cadastrado com sucesso!",
                    type: "success",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Projeto/listaProjetos/' . $cliente[0]->idUsuario); ?>";
                        });
            </script>
        <?php else: ?>
            <script>
                swal({
                    title: "",
                    text: "Erro ao cadastrar Projeto!",
                    type: "error",
                },
                        function () {
                            window.location.href = "<?php echo base_url('admin/Projeto/listaProjetos/' . $cliente[0]->idUsuario); ?>";
                        });
            </script>
        <?php endif; ?>
    <?php endif; ?>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php
            echo "Projetos do Cliente ";
            echo isset($cliente[0]) ? $cliente[0]->nome : "";
            ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>">Inicio</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> 
                <?php
                echo "Projetos do Cliente ";
                echo isset($cliente[0]) ? $cliente[0]->nome : "";
                ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo base_url('admin/cliente'); ?>" class="btn btn-primary">Voltar</a>
            </div>
        </div>

        <table id="tablemensagens" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Projetos</th>
                    <th>Data Inicio</th>
                    <th>Data Prevista Para Término</th>
                    <th>Concluído</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($projetos as $projeto): ?>
                    <tr>

                        <td><?php echo $projeto->nome; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($projeto->dataInicio)); ?></td>
                        <td><?php echo date('d/m/Y', strtotime($projeto->dataPrevistaFim)); ?></td>
                        <td><?php echo $projeto->flFinalizado ? "Sim" : "não"; ?></td>
                        <td>
                            <a href="<?php echo base_url('admin/Tarefa/listaTarefas/' . $projeto->idProjeto); ?>">
                                <span class="fa fa-folder-open" title="Projeto do Cliente" style="color: green;"></span>
                                <a class="javascript:void(0)" onclick="excluir('<?= $projeto->idProjeto ?>', '<?= $projeto->nome ?>')"><span class="glyphicon glyphicon-trash" title="Excluir usuário" style="color: black;"></span></a>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <div align="right">
            <a href="<?php echo base_url('admin/Projeto/novoprojeto/' . $cliente[0]->idUsuario); ?>" class="btn btn-primary">Cadastrar Projeto</a>
        </div>

        <script>
            function excluir(idProjeto, tarefa) {
                confirmecao = confirm('Deseja realmente excluir o projeto ' + tarefa);

                if (confirmecao == true) {
                    window.location.href = "<?= base_url('admin/Projeto/excluir/' ) ?>" + idProjeto+"/"+<?=$cliente[0]->idUsuario?>;
                }
            }
        </script>