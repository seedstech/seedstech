<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $titulo; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>">Inicio</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> <?php echo $titulo; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <h3>
                    <?php
                    echo "Novo Projeto para o cliente ";
                    echo isset($cliente[0]) ? $cliente[0]->nome : "";
                    ?>
                </h3>   
            </div>

        </div>



        <div class="row">
            <div class="col-md-3"></div>

            <div class="col-md-6">
                <form action="<?php echo base_url('admin/projeto/cadastrar');?>" method="post">
                    <input type="hidden" value="<?php echo $cliente[0]->idUsuario; ?>" name="idCliente">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input id="nome" type="text" name="nome" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label for="dataInicio">Data Prevista para Início</label>
                        <input id="dataInicio" type="date" name="dataInicio" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label for="dataPrevistaFim">Data Prevista para Término</label>
                        <input id="dataPrevistaFim" type="date" name="dataPrevistaFim" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label for="cliente">Cliente</label>
                        <input id="cliente" type="text" name="cliente" class="form-control" placeholder="<?php echo $cliente[0]->nome ?>" disabled>
                    </div>

                    <div class="form-group">
                        <div class="pull-right btn-alinhado-direita">
                            <button type="submit" class="btn btn-success pull-right pa">Cadastrar</button>
                        </div>
                        <div class="pull-right btn-alinhado-direita">
                            <button type="reset" class="btn btn-warning pull-right">Cancelar</button>
                        </div>
                        <div class="pull-right btn-alinhado-direita">
                            <a href="<?php echo base_url('admin/projeto/listaProjetos/' . $cliente[0]->idUsuario); ?>" class="btn btn-default pull-right">Voltar</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        </script>

        <style>
            .btn-alinhado-direita{
                padding-left: 10px;
            }
        </style>

