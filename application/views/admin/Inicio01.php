<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">

        <title>Seeds Tech - Soluções em Informática</title>
        <link rel="icon" href="<?php echo base_url('assest/icone32x32.png'); ?>" type="image/png">
        <link rel="shortcut icon" href="<?php echo base_url('assets/icone32x32.ico'); ?>" type="img/x-icon">

        <!-- 
        <link rel="icon" href="favicon.png" type="image/png">
        <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">
        -->

        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/font-awesome.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/responsive.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/login.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/admin.css"); ?>" rel="stylesheet" type="text/css">

        <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.1.8.3.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-scrolltofixed.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.easing.1.3.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.isotope.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/wow.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/classie.js"); ?>"></script>
        <script src="<?php echo base_url("assets/contactform/contactform.js"); ?>"></script>

    </head>
    <body>
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>Menu
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll " href="<?php echo base_url("login"); ?>">
                        <img id="LogoNavBar" src="<?php echo base_url("assets/img/logonavbar.png"); ?>" alt=""> Bem vindo <?php echo $nome . " ! ";?>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" style="font-size: 16px; padding-right: 40px; padding-top: 15px;">
                        <li><a href="<?php echo base_url("admin/usuario"); ?>">Cadastro de Usuário</a></li>
                        <li><a href="<?php echo base_url("login/sair"); ?>">Sair</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <div>
            <h2>Você já está logado como Administrador do Sistema</h2>
        </div>

    </body>
</html>

