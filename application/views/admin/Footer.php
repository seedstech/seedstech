</section>
</div>           

<script src="<?php echo base_url('assets/js/util/mascaras.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/mask/jquery.mask.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/mask/jquery.maskMoney.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/mask/jquery.maskedinput-1.3.min.js'); ?>" type="text/javascript"></script>


<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/template/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/template/inc/jquery-ui.min.js" type="text/javascript'); ?>"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/template/bootstrap/js/bootstrap.min.js'); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/template/inc/raphael-min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/template/plugins/morris/morris.min.js'); ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/template/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/template/plugins/knob/jquery.knob.js'); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/template/inc/moment.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/template/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/template/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/template/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/template/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/template/dist/js/app.min.js'); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/template/dist/js/pages/dashboard.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/template/dist/js/demo.js'); ?>"></script>
</body>
</html>


