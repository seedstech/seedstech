<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li class="header">Menu</li>
            <li><a href="<?php echo base_url("admin"); ?>"><i class="fa fa-comments-o"></i> <span>Caixa de Mensagens</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i> <span>Cadastro</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="<?php echo base_url("admin/Usuario"); ?>">
                            <i class="fa fa-circle-o"></i>Administrador
                        </a>
                    </li>
                    <li><a href="<?php echo base_url("admin/Cliente"); ?>"><i class="fa fa-circle-o"></i>Clientes</a></li>
                </ul>
            </li>
    </section>
    <!-- /.sidebar -->
</aside>



