<?php if (isset($sucesso)): ?>
    <?php if ($sucesso == 1): ?>
        <script>
            swal({
                title: "",
                text: "Cliente cadastrado com sucesso!",
                type: "success"
            }, function () {
                window.location.href = "<?= base_url('admin/Cliente') ?>";
            });
        </script>
    <?php else: ?>
        <script>
            swal({
                title: "",
                text: "Erro ao cadastrar cliente!",
                type: "error"
            }, function () {
                window.location.href = "<?= base_url('admin/Cliente') ?>";
            });
        </script>
    <?php endif; ?>
<?php endif; ?>


<?php if (isset($sucessoExcluir)): ?>
    <?php if ($sucessoExcluir == 1): ?>
        <script>
            swal({
                title: "",
                text: "Cliente excluído com sucesso!",
                type: "success"
            }, function () {
                window.location.href = "<?= base_url('admin/Cliente') ?>";
            });
        </script>
    <?php else: ?>
        <script>
            swal({
                title: "",
                text: "Erro ao excluir cliente!",
                type: "error"
            }, function () {
                window.location.href = "<?= base_url('admin/Cliente') ?>";
            });
        </script>
    <?php endif; ?>
<?php endif; ?>


<!-- Aqui começa o cabeçalho com barra de navegação da página-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $titulo; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-cogs"></i> <?php echo $titulo; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
        <!-- Aqui começa de fato o conteúdo da página-->
        <section class="content">

            <table id="tableclientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Email</th>
                        <th>Usuário</th>
                        <th>Senha</th>
                        <th>Situação</th>
                        <th>Projetos</th>
                        <th>Excluir</th>
                    </tr>
                </thead>

                <tbody>

                    <?php foreach ($clientes as $cliente) : ?>
                        <tr>
                            <td><?php echo $cliente->nome; ?></td>
                            <td><?php echo $cliente->telefone; ?></td>
                            <td><?php echo $cliente->email; ?></td>
                            <td><?php echo $cliente->usuario; ?></td>
                            <td>****</td>

                            <?php if ($cliente->flAtivo) { ?>
                                <td>Ativo</td>
                            <?php } else { ?>
                                <td>Inativo</td>
                            <?php } ?>

                            <td>
                                <a href="<?php echo base_url('admin/Projeto/listaProjetos/' . $cliente->idUsuario); ?>">
                                    <span class="fa fa-folder-open" title="Projeto do Cliente" style="color: green;"></span>
                                </a>
                            </td>

                            <td class="text-center">
                                <a href="<?php echo base_url('admin/Usuario/excluirCliente/' . $cliente->idUsuario); ?>"><span class="glyphicon glyphicon-trash" title="Excluir usuário" style="color: black;"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            <br>
            <div align="right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modaladdusuario">Cadastrar Cliente</button>
            </div>

            <!-- Modal de confirmação de exclusão de usuário da tabela-->
            <div class="modal fade" id="modaldelusuario" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Atenção!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Deseja sair?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Não!</button>
                            <a href="<?php echo base_url('login/sair'); ?>"><button type="button" class="btn btn-success" >Sim, quero sair!</button></a>
                        </div>
                    </div>
                </div>
            </div>



            <!--Modal de Cadastro de Usuário-->

            <div class="modal fade" id="modaladdusuario" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Cadastro de Cliente</h4>
                        </div>
                        <form id="formCadCliente" action="<?= base_url('admin/Usuario/cadastrarCliente') ?>" method="post" onsubmit="return validar()">
                            <div class="modal-body">

                                <p>Favor preencher os campos abaixo para cadastrar novo Cliente:</p>

                                <div id="campoNome" class="form-group"><!--has-error-->
                                    <input id = "cnome" type="text" name="nome"class="form-control somenteLetras" placeholder="Nome" autofocus="" required="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoTelefone" class="form-group">
                                    <input id = "ctelefone" type="text" name="telefone"class="form-control maskTelefone" placeholder="Telefone" required="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoEmail" class="form-group">
                                    <input id = "cemail" type="text" name="email"class="form-control" placeholder="Email" required="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoUsuario" class="form-group">
                                    <input id = "cusuario" type="text" name="usuario" class="form-control somenteLetras" placeholder="Usuário" required="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoSenha" class="form-group">
                                    <input id = "csenha" type="password" name="senha"class="form-control" placeholder="Senha" required="">
                                    <span class="help-block"></span>
                                </div>

                                <div id="campoConfirmaSenha" class="form-group">
                                    <input id = "cconfirmaSenha" type="password" name="confirmarSenha"class="form-control" placeholder="Confirmar Senha" required="">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                <button type="submite" class="btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

<script>
    function validar() {
        senha = document.getElementById('csenha').value;
        confirmaSenha = document.getElementById('cconfirmaSenha').value;

        if (senha != confirmaSenha) {
            swal({
                title: "",
                text: "As senhas estão diferentes!!",
                type: "error"
            }, function () {
            });
        } else {
            return true;
        }
        return false;
    }
</script>


