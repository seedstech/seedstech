<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $titulo ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin"); ?>"> Início</a></li>
            <li class="active"><i class="fa fa-comments-o"></i> <?= $titulo ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">



        <form id="form-email" action="" method="post">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="destinatario">destinatário</label>
                        <input class="form-control" type="text" id="destinatario" name="destinatario" disabled="" value="<?= $contato->nome ?>"> 
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="mail">E-mail</label>
                        <input class="form-control" type="text" id="email" name="email" disabled="" value="<?= $contato->email ?>">                     
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="assunto">Assunto</label>
                <input class="form-control" type="text" id="assunto" name="assunto" value="Resposta a solicitação de contato da Seeds Tech">  
            </div>
            <div class="form-group">
                <label for="mensagem">Mensagem</label>
                <textarea id="mensagem" name="mensagem" class="form-control"><?= "<b>Mensagem:</b> " . $contato->descricao . "<br><b>Sr(a) " . $contato->nome . "</b>, " ?></textarea>
            </div>

            <div class="pull-right">
                <button id="enviar" type="submit" class="btn btn-success pull-right">Enviar</button>
                <a href="<?= base_url() ?>" class="btn pull-right">Voltar</a>
            </div>

            <div style="clear: both"></div>

        </form>

        <script src="<?= base_url('assets/template/plugins/ckeditor/ckeditor.js') ?>"></script>
        <script>
            CKEDITOR.replace('mensagem');
        </script>

        <script>
            $c = jQuery.noConflict();



            $c("#form-email").submit(function(event) {
                var email = {
                    destinatario: $c("#destinatario").val(),
                    email: $c("#email").val(),
                    assunto: $c("#assunto").val(),
                    mensagem: CKEDITOR.instances['mensagem'].getData()
                };

                $c("#enviar").attr('disabled','true');

                $c.post("<?= base_url('contato/responder') ?>", email, function(data) {
                    tipoAlerta = "";

                    //verifica se o Json data tem um status igual a 1, 
                    //caso o cadastro tenha sido feito, caso contrário, é igual a 0
                    if (data.status == 1) {
                        tipoAlerta = "success";

                        $c('#mensagem').val("");
                        $c('#assunto').val("");

                    } else {
                        tipoAlerta = "error";
                    }

                    //alerta personalizado sem título (caso de dúvida 
                    //verificar documentação do plugin sweetAlert na web)
                    swal("", data.msg, tipoAlerta);

                    $c("#enviar").removeAttr('disabled');

                }, 'json');
                event.preventDefault();
            });
        </script>


