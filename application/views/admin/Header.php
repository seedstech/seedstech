
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Seeds Tech - Soluções em Informática</title>
        <link rel="icon" href="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>" type="image/png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <title>Seeds Tech</title>
        
        <!-- Importação da DataTable-->
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.12.4.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/query.dataTables.min.js"); ?>"></script>

        
        
        
        <link rel="shortcut icon" href="<?php echo base_url('assets/templateInicio/img/logo27x27.png'); ?>" type="img/x-icon">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
            <link rel="stylesheet" href="<?php echo base_url('assets/template/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/inc/font-awesome.min.css'); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/inc/ionicons.min.css'); ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/dist/css/AdminLTE.min.css'); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/dist/css/skins/_all-skins.min.css'); ?>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/iCheck/flat/blue.css'); ?>">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/morris/morris.css'); ?>">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/datepicker/datepicker3.css'); ?>">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/daterangepicker/daterangepicker.css'); ?>">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?php echo base_url('assets/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">


        <script type="text/javascript" src="<?php echo base_url("assets/js/util/Requisicoes.js"); ?>"></script>

        <!-- arquivos para exibição de alerta personalizados-->
        <script src="<?php echo base_url("assets/alertas/sweetalert.min.js"); ?>" type="text/javascript"></script>
        <link href="<?php echo base_url("assets/alertas/sweetalert.css"); ?>" rel="stylesheet" type="text/css"/>

        <!-- configurações de constantes do arquivo assets/util/requisicoes.js-->
        <script type="text/javascript">
            baseUrl = "<?php echo base_url(); ?>";
        </script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        
        
        
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <img style="width: 28px; height: 100%; margin-top: -5px" src="<?php echo base_url('assets/icone32x32.png'); ?>" alt=""/>
                    </span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <img style="width: 28px; height: 100%; margin-top: -5px" src="<?php echo base_url('assets/icone32x32.png'); ?>" alt=""/>
                        cPanel</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url('assets/template/dist/img/user2-160x160.jpg'); ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $nome; ?></span>
                                </a>
                                <ul class="dropdown-menu">

                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url('assets/template/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo $nome; ?>
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="" class="btn btn-default btn-flat"  data-toggle="modal" data-target="#myModal">Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Atenção!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Deseja sair?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Não!</button>
                            <a href="<?php echo base_url('login/sair'); ?>"><button type="button" class="btn btn-success" >Sim, quero sair!</button></a>
                        </div>
                    </div>
                </div>
            </div>


