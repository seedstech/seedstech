<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">

        <title>Seeds Tech - Soluções em Informática</title>
        <link rel="icon" href="<?php echo base_url('assest/icone32x32.png'); ?>" type="image/png">
        <link rel="shortcut icon" href="<?php echo base_url('assets/icone32x32.ico'); ?>" type="img/x-icon">

        <!-- 
        <link rel="icon" href="favicon.png" type="image/png">
        <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">
        -->

        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/font-awesome.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/responsive.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/animate.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/login.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("assets/css/admin.css"); ?>" rel="stylesheet" type="text/css">

        <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.1.8.3.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-scrolltofixed.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.easing.1.3.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.isotope.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/wow.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/classie.js"); ?>"></script>
        <script src="<?php echo base_url("assets/contactform/contactform.js"); ?>"></script>

    </head>
    <body>
        <div id="fullscreen_bg" class="fullscreen_bg"/>
        <div class="container">
            <form class="form-signin animated fadeInDown delay-06s" action="<?php echo base_url("login/entrar"); ?>" method="post">
                <a href="index.php"><div><h1 ><img id="LogoSeedsLogin" src="<?php echo base_url("assets/img/logoseeds.png"); ?>" alt=""></h1></div></a>
                <hr>
                <input type="text" name="usuario"class="form-control" placeholder="Email" required="" autofocus="" value="<?php echo set_value('usuario'); ?>">
                <?php echo form_error('usuario', "<div>", "</div>"); ?>
                <input type="password" name="senha" class="form-control" placeholder="Senha" required="" value="<?php echo set_value('senha'); ?>">
                <?php echo form_error('senha', "<div>", "</div>"); ?>
                <button class="btn btn-lg btn-success btn-block" type="submit">
                    Entrar
                </button>
            </form>
            <center>
                <a href="index.php">
                    <button id="voltarinicio"class="btn btn-invert  btn-block" >
                        Início
                    </button>
                </a>
            </center>
        </div>
    </body>
</html>