<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Contato
 *
 * @author Helder dos Santos
 */
class Contato extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("ContatoModel", "mContato");
    }

    public function index() {
        $this->load->view("contato");
    }

    /**
     * imprime Json para ser usado em requisições AJAX
     * @param type $msg
     * @param type $status => "1 para verdadeiro e 0 para falso"
     */
    private function imprimirMsgJson($msg, $status) {
        $result = array("msg" => $msg, "status" => $status);

        echo json_encode($result);
    }

    public function adicionar() {

        if ($this->input->post("nome") != "" && $this->input->post("telefone") != "" && $this->input->post("descricao") != "") {
            $contato = array(
                "nome" => $this->input->post("nome"),
                "telefone" => $this->input->post("telefone"),
                "email" => $this->input->post("email"),
                "descricao" => $this->input->post("descricao"),
                "flReceberEmail" => $this->input->post('receberEmail') == "on" ? "true" : "false"
            );

            $result = $this->mContato->adicionar($contato);

            if ($result === true) {
                $this->imprimirMsgJson("Contato salvo com sucesso! Entraremos em contato em breve!", "1");
            } else {
                $this->imprimirMsgJson("Desculpe, houve um erro ao salvar contato!", "1");
            }
        } else {
            $this->imprimirMsgJson("Campos com asterisco(*) são obrigatório!", "0");
        }
    }

    public function formEmail($idContato) {
        verificarNivelAdm($this->session->userdata());

        $this->load->model("UsuarioModel", "mUsuario");

        $contato = $this->mContato->BuscarPorId($idContato);

        if (count($contato) > 0) {
            $dados['clientes'] = $this->mUsuario->listarTodos(true);
            $dados['nome'] = $this->session->userdata('nome');
            $dados['titulo'] = "Responder Email - " . $contato[0]->nome;
            $dados['contato'] = $contato[0];

            $this->load->view("includesAreaRestrita/Header", $dados);
            $this->load->view("admin/Menu", $dados);
            $this->load->view("admin/ResponderEmail", $dados);
            $this->load->view("includesAreaRestrita/Footer");
        } else {
            redirect(base_url());
        }
    }
    
    
    public function responder() {
        verificarNivelAdm($this->session->userdata());

        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');
        $destinatario = $this->input->post('email');

        if ($assunto != '' && $mensagem != '' && $destinatario != '') {
            if ($this->enviarEmail($assunto, $mensagem, $destinatario)) {
                $this->imprimirMsgJson('Email enviado com sucesso', '1');
            } else {
                $this->imprimirMsgJson('Erro ao enviar e-mail', '0');
            }
        } else {
            $this->imprimirMsgJson('Todos os campos são obrigatórios!', '0');
        }
    }

    /**
     * Enviar e-mail com EMTP
     * @param type $assunto
     * @param type $mensagem
     * @param type $destinatario
     * @return type
     */
    private function enviarEmail($assunto, $mensagem, $destinatario) {
        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'mail.seedstech.com.br';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'contato@seedstech.com.br';
        $config['smtp_pass'] = 'STe-mail491';

        $this->email->initialize($config);

        $this->email->from('contato@seedstech.com.br', 'Seeds Tech');
        $this->email->to($destinatario);

        $this->email->subject($assunto);
        $this->email->message($mensagem);


        return $this->email->send();
    }
    
    /**
     * Abrir visualização das respostas de um contato
     * @param type $idContato
     */
    function abrir($idContato){
        verificarNivelAdm($this->session->userdata());
        
        echo "<h1>id do Contato: ".$idContato." -> Falta implementar.</h1>";
    }
    
    /**
     * exclui o contato da base dados
     */
    function excluir(){        
        verificarNivelAdm($this->session->userdata());
        
        if($this->input->post('contato') != ''){
            if($this->mContato->excluir($this->input->post('contato'))){
                 $this->imprimirMsgJson("Contato excluído com sucesso!", 1);
            }else{
                 $this->imprimirMsgJson("Erro ao excluir contato!", 0);
            }
        }else{
            $this->imprimirMsgJson("Erro ao excluir contato!", 0);
        }
        
    }

}
