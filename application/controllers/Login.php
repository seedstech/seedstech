<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Login
 *
 * @author Helder dos Santos
 */
class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('encrypt');
        $this->load->model("UsuarioModel", "mUsuario");
    }

    /**
     * Abre por padrão a página de login
     */
    public function index() {
        //verifica se tem um usuario logado
        if ($this->session->userdata('usuarioLogado')) {
            redirect(base_url("admin"));
        }

        $this->load->view("Login2");
    }

    /**
     * Faz login no sistema
     */
    public function entrar() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('usuario', 'Usuario', 'required', array('required' => 'Usuário é obrigatório.'));
        $this->form_validation->set_rules('senha', 'Senha', 'required', array('required' => 'Senha é obrigatória.'));

        //verificar se os dados informados estão de acordo com a validação necessária
        if ($this->form_validation->run() === true) {

            $usuarioBusca = $this->mUsuario->buscarPorUsuario($this->input->post('usuario'));
            //verifica se na busca foi encontrado algum usuario com o nome informado
            if (count($usuarioBusca) >= 1) {

                //decodifica a enha e verifica se tá igual a senha informada
                if (password_verify($this->input->post("senha"), $usuarioBusca[0]->senha)) {
                //if ($this->encrypt->decode($usuarioBusca[0]->senha) == $this->input->post("senha")) {

                   $dadosSessao = array(
                        'idUsuario' => $usuarioBusca[0]->idUsuario,
                        'nome' => $usuarioBusca[0]->nome,
                        'usuario' => $usuarioBusca[0]->usuario,
                        'usuarioLogado' => true,
                        'flCliente' =>$usuarioBusca[0]->flCliente
                    );

                    //inicia um sessão com o dados encontrados
                    $this->session->set_userdata($dadosSessao);
                    redirect(base_url("admin"));
                } else {
                    $dados['erro'] = "Usuário ou senha incorreta!";
                    $this->load->view("Login2", $dados);
                }
            } else {
                $dados['erro'] = "Usuário ou senha incorreta!";
                $this->load->view("Login2", $dados);
            }
        } else {
            $erros = array('mensagens' => validation_errors());
            $this->load->view('Login2', $erros);
        }
    }

    /**
     * Fecha a sessão e sai página do usuário
     */
    function sair() {
        $this->session->sess_destroy();
        redirect(base_url("login"));
    }

}
