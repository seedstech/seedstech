<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Usuario
 *
 * @author Helder dos Santos
 */
class Usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('encrypt');
        $this->load->model("UsuarioModel", "mUsuario");

        verificarNivelAdm($this->session->userdata());
    }

    public function index() {

        $dados['usuarios'] = $this->mUsuario->listarTodos();
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Cadastro de Usuário";

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/CadastroUsuario", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

    public function PaginaAdmAlterar($idUsuario) {
        echo "Id Usuário: " . $idUsuario . "<h1>falta implementar</1>";
    }

    public function cadastrar() {
        //$this->verificarNivelAdmin();
        //$flAdministrador = $valor = $this->input->post('flAdministrador') == "on" ? "true" : "false";

        $usuario = array(
            "nome" => $this->input->post('nome'),
            "telefone" => $this->input->post('telefone'),
            "email" => $this->input->post('email'),
            "usuario" => $this->input->post('usuario'),
            "senha" => password_hash($this->input->post('senha'), PASSWORD_DEFAULT)
                //"senha" => $this->encrypt->encode($this->input->post('senha')),
        );

        if ($this->mUsuario->adicionar($usuario)) {
            echo "Usuário cadastrado com sucesso!";
        } else {
            echo "Erro ao cadastrar usuário!!";
        }
    }

    public function cadastrarCliente() {
        //$this->verificarNivelAdmin();
        //$flAdministrador = $valor = $this->input->post('flAdministrador') == "on" ? "true" : "false";

        $usuario = array(
            "nome" => $this->input->post('nome'),
            "telefone" => $this->input->post('telefone'),
            "email" => $this->input->post('email'),
            "usuario" => $this->input->post('usuario'),
            "senha" => password_hash($this->input->post('senha'), PASSWORD_DEFAULT),
            "flCliente" => true
                //"senha" => $this->encrypt->encode($this->input->post('senha')),
        );

        if ($this->mUsuario->adicionar($usuario)) {
            $dados['sucesso'] = true;
        }

        $dados['clientes'] = $this->mUsuario->listarTodos(true);
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Cadastro de Clientes";

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/CadastroCliente", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

    public function excluirCliente($idUsuario) {
        $dados['sucessoExcluir'] = $this->mUsuario->excluir($idUsuario);
        
        $dados['clientes'] = $this->mUsuario->listarTodos(true);
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Cadastro de Clientes";

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/CadastroCliente", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

}
