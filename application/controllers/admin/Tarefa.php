<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Tarefa
 *
 * @author Helder dos Santos
 */
class Tarefa extends CI_Controller {

    function __construct() {
        parent::__construct();

        verificarNivelAdm($this->session->userdata());
        $this->load->model('TarefaModel', 'mTarefa');
        $this->load->model('ProjetoModel', 'mProjeto');
    }

    function index() {
        redirect(base_url());
    }

    function listaTarefas($idProjeto, $mensagem = null) {
        $projeto = $this->mProjeto->buscarPorId($idProjeto);

        if ($projeto != null) {

            $dados['nome'] = $this->session->userdata('nome');
            $dados['titulo'] = "Tarefas do Projeto - " . $projeto[0]->nome;
            $dados['tarefas'] = $this->mTarefa->buscarTodas($idProjeto);
            $dados['idCliente'] = $projeto[0]->idCliente;
            $dados['idProjeto'] = $projeto[0]->idProjeto;

            if ($mensagem != null && $mensagem === "1" || $mensagem === "0") {
                $dados['sucesso'] = $mensagem;
            }

            $this->load->view("includesAreaRestrita/Header", $dados);
            $this->load->view("admin/Menu", $dados);
            $this->load->view("admin/projetos/TarefasProjeto", $dados);
            $this->load->view("includesAreaRestrita/Footer");
        } else {
            redirect(base_url());
        }
    }

    function novaTarefa($idProjeto) {
        $projeto = $this->mProjeto->buscarPorId($idProjeto);

        if ($projeto != null) {
            $dados['nome'] = $this->session->userdata('nome');
            $dados['titulo'] = "Tarefas do Projeto - " . $projeto[0]->nome;
            $dados['tarefas'] = $this->mTarefa->buscarTodas($idProjeto);
            $dados['projeto'] = $projeto;

            $this->load->view("includesAreaRestrita/Header", $dados);
            $this->load->view("admin/Menu", $dados);
            $this->load->view("admin/projetos/NovaTarefa", $dados);
            $this->load->view("includesAreaRestrita/Footer");
        } else {
            redirect(base_url());
        }
    }

    function cadastrar() {
        $resposta = $this->mTarefa->adicionar($this->input->post());

        if ($resposta) {
            redirect(base_url('admin/tarefa/listaTarefas/' . $this->input->post('idProjeto') . '/1'));
        } else {
            redirect(base_url('admin/tarefa/listaTarefas/' . $this->input->post('idProjeto') . '/0'));
        }
    }

    public function excluir($idProjeto, $idTarefa) {
        $resposta = $this->mTarefa->excluir($idTarefa);

        $projeto = $this->mProjeto->buscarPorId($idProjeto);

        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Tarefas do Projeto - " . $projeto[0]->nome;
        $dados['tarefas'] = $this->mTarefa->buscarTodas($idProjeto);
        $dados['idCliente'] = $projeto[0]->idCliente;
        $dados['idProjeto'] = $projeto[0]->idProjeto;

        $dados['sucessoExcluir'] = $resposta;


        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/projetos/TarefasProjeto", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }
    
    public function finalizar($idProjeto, $idTarefa){
        $this->mTarefa->finalizar($idTarefa);
        
        redirect(base_url('admin/Tarefa/listaTarefas/'.$idProjeto));
        
    }

}
