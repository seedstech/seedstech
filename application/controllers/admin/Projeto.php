<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of TarefasProjetos
 *
 * @author Heldos dos Santos
 */
class Projeto extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('UsuarioModel', 'mUsuario');
        $this->load->model('ProjetoModel', 'mProjeto');
        verificarNivelAdm($this->session->userdata());
    }

    function index() {
        redirect(base_url('admin/cliente'));
    }

    function listaProjetos($idCliente, $mensagem = null) {
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Projetos";
        $dados['cliente'] = $this->mUsuario->buscarPorId($idCliente);
        $dados['projetos'] = $this->mProjeto->buscarTodos($idCliente);

        if ($mensagem != null && $mensagem === "1" || $mensagem === "0") {
            $dados['sucesso'] = $mensagem;
        }

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/projetos/ProjetosCliente", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

    function novoProjeto($idCliente) {
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Novo Projetos";
        $dados['cliente'] = $this->mUsuario->buscarPorId($idCliente);

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/projetos/NovoProjeto", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

    function cadastrar() {
        if ($this->mProjeto->adicionar($this->input->post())) {
            redirect(base_url('admin/Projeto/listaProjetos/' . $this->input->post('idCliente') . '/' . true));
        } else {
            redirect(base_url('admin/Projeto/listaProjetos/' . $this->input->post('idCliente') . '/' . false));
        }
    }

    public function excluir($idProjeto, $idCliente) {
        $resposta = $this->mProjeto->excluir($idProjeto);

        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Projetos";
        $dados['cliente'] = $this->mUsuario->buscarPorId($idCliente);
        $dados['projetos'] = $this->mProjeto->buscarTodos($idCliente);

        $dados['sucessoExcluir'] = $resposta;

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/projetos/ProjetosCliente", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

}
