<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Cliente
 *
 * @author Heldos dos Santos
 */
class Cliente extends CI_Controller{
    
    public function __construct() {
        parent::__construct();

        verificarNivelAdm($this->session->userdata());
        
        $this->load->library('encrypt');
        $this->load->model("UsuarioModel", "mUsuario");        
    }

    public function index() {
        
        $dados['clientes'] = $this->mUsuario->listarTodos(true);
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Cadastro de Clientes";

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/CadastroCliente", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

    public function PaginaAdmAlterar($idUsuario) {
        echo "Id Usuário: ".$idUsuario."<h1>falta implementar</1>";
    }


    public function cadastrar() {
        //$this->verificarNivelAdmin();
        //$flAdministrador = $valor = $this->input->post('flAdministrador') == "on" ? "true" : "false";

        $usuario = array(
            "nome" => $this->input->post('nome'),
            "telefone" => $this->input->post('telefone'),
            "email" => $this->input->post('email'),
            "usuario" => $this->input->post('usuario'),
            "senha" => $this->encrypt->encode($this->input->post('senha')),
            "flCliente" => true
        );

        if ($this->mUsuario->adicionar($usuario)) {
            echo "Cliente cadastrado com sucesso!";
        } else {
            echo "Erro ao cadastrar cliente!";
        }
    }

    public function alterar() {

        echo "Falta Implementar";
    }

    public function excluir($idUsuario) {

        echo "Id Usuário: ".$idUsuario."<h1>falta implementar</1>";
    }
}
