<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ContasAPagar
 *
 * @author Helder dos Santos
 */
class ContasAPagar extends CI_Controller{
   
    public function __construct() {
        parent::__construct();
        verificarNivelAdm($this->session->userdata());
    }
    
    public function index(){
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Página Inicial";
        //$this->load->view("admin/inicio", $dados);
        
        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/financeiro/ContasAPagar", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }
}
