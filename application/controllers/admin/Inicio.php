<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Inicio
 *
 * @author Helder dos Santos
 */
class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ContatoModel','mContato');
        verificarNivelAdm($this->session->userdata());
    }

    public function index() {
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Página Inicial";
        $dados['mensagens'] = $this->mContato->buscarNaoRespondidas();

        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("admin/Menu", $dados);
        $this->load->view("admin/Inicio", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }

}
