<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe incial que deverá conter todas as funções 
 * necessárias a serem executadas na página inicial do sistema
 * 
 * @author Helder dos Santos
 */
class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('usuarioLogado')) {
            redirect(base_url("admin"));
        }
    }

    public function index() {
        $this->load->view("Inicio2");
    }

}
