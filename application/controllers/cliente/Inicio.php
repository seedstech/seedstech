<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Inicio
 *
 * @author Helder dos Santos
 */
class Inicio extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        verificarNivelCliente($this->session->userdata());
        $this->load->model('ProjetoModel', 'mProjeto');
    }
    
    public function index(){
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Página Inicial";
        $dados['projetos'] = $this->mProjeto->buscarPorCliente($this->session->userdata('idUsuario'));
        
        
        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("cliente/Menu", $dados);
        $this->load->view("cliente/Inicio", $dados);
        $this->load->view("includesAreaRestrita/Footer");
        
    }

}