<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Atendimento
 *
 * @author Heldos dos Santos
 */
class Atendimento extends CI_Controller{
    public function __construct() {
        parent::__construct();
        
        verificarNivelCliente($this->session->userdata());
    }
    
    function index(){
        $dados['nome'] = $this->session->userdata('nome');
        $dados['titulo'] = "Abrir Chamado";        
        
        $this->load->view("includesAreaRestrita/Header", $dados);
        $this->load->view("cliente/Menu", $dados);
        $this->load->view("cliente/Atendimento", $dados);
        $this->load->view("includesAreaRestrita/Footer");
    }
}
