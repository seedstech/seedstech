//definição de todas as variáveis globais referentes a requisições AJAX devem ser feitas nesta área
var baseUrl;
var $j = jQuery.noConflict();

$j(document).ready(function () {

    /**
     * Função com requisição AJAX para salvar um contato
     */
    $j("#form-contato").submit(function (event) {
        /*
         * Cria um objeto com os atributo a serem enviados
         */
        
        $j('#form-contato .carregando-form').css({'display':'block'});
        var contato = {
            nome: $j("#nome").val(),
            telefone: $j("#telefone").val(),
            email: $j("#email").val(),
            descricao: $j("#descricao").val(),
            receberEmail: $j("#receberEmail").is(':checked')
        };

        /*
         * envia uma requisição (AJAX sem recarregar a página) via post para o controller contato 
         * na função adicionar e espera um json de reposta
         * com a mensagem e o status
         */
        
        $j.post(baseUrl + "contato/adicionar", contato, function (data) {
            tipoAlerta = "";

            //verifica se o Json data tem um status igual a 1, 
            //caso o cadastro tenha sido feito, caso contrário, é igual a 0
            if (data.status == 1) {
                tipoAlerta = "success";

                $j('#form-contato input').val("");
                $j('#form-contato textarea').val("");
                
            } else {
                tipoAlerta = "error";
            }
            
            $j('#form-contato .carregando-form').css({'display':'none'});

            //alerta personalizado sem título (caso de dúvida 
            //verificar documentação do plugin sweetAlert na web)
            swal("", data.msg, tipoAlerta);

        }, 'json');

        //evita que a página seja recarregada
        event.preventDefault();
    });


    /**
     * Requisição do formulário da cadastro de usuario
     */
    $j("#formCadUsuario").submit(function (event) {

        var valido = true;

        if ($("#nome").val() == "") {
            $("#campoNome").addClass("has-error");
            $("#campoNome span").empty();
            $("#campoNome span").append("Informe o Nome");
            valido = false;
        }

        if ($("#telefone").val() == "") {
            $("#campoTelefone").addClass("has-error");
            $("#campoTelefone span").empty();
            $("#campoTelefone span").append("Informe um telefone de contato");
            valido = false;
        }

        if ($("#email").val() == "") {
            $("#campoEmail").addClass("has-error");
            $("#campoEmail span").empty();
            $("#campoEmail span").append("Informe um E-mail de contato");
            valido = false;
        }

        if ($("#usuario").val() == "") {
            $("#campoUsuario").addClass("has-error");
            $("#campoUsuario span").empty();
            $("#campoUsuario span").append("Informe usuario");
            valido = false;
        }

        if ($("#senha").val() == "") {
            $("#campoSenha").addClass("has-error");
            $("#campoSenha span").empty();
            $("#campoSenha span").append("Informe uma senha");
            valido = false;
        }

        if ($("#confirmaSenha").val() == "") {
            $("#campoConfirmaSenha").addClass("has-error");
            $("#campoConfirmaSenha span").empty();
            $("#campoConfirmaSenha span").append("Confirme senha");
            valido = false;
        }

        if ($("#confirmaSenha").val() != $("#senha").val()) {
            $("#campoConfirmaSenha").addClass("has-error");
            $("#campoConfirmaSenha span").empty();
            $("#campoConfirmaSenha span").append("As senhas etão diferentes");
            valido = false;
        }



        if (valido) {
            var usuario = {
                nome: $j("#nome").val(),
                telefone: $j("#telefone").val(),
                email: $j("#email").val(),
                usuario: $j("#usuario").val(),
                senha: $j("#senha").val(),
                administrador: $j("administrador").is(':checked')
            }

            $j.post(baseUrl + "admin/usuario/cadastrar", usuario, function (data) {

            }, 'json');
        }


        event.preventDefault();
    });

    /**
     * Permite que o suário digite omente letras
     */
    $j('.somenteLetras').keyup(function () {
        this.value = this.value.replace(/[^a-zA-Z ]/g, '');
    });
})

function addErroEmInput(idInput) {

}



