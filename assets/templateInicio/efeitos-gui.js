    //Script para botão voltar ao topo
    var conf = jQuery.noConflict();
    
    conf(document).ready(function(){
        conf('.voltar-topo').hide();

        conf(window).scroll(function(){

          if(conf(this).scrollTop() > 100){
              conf('.voltar-topo').fadeIn();
          }else{
            conf('.voltar-topo').fadeOut();
          }

        });

        conf('.voltar-topo').click(function(){
          conf('html, body').animate({
            scrollTop: 0
          }, 400);
        });

    });